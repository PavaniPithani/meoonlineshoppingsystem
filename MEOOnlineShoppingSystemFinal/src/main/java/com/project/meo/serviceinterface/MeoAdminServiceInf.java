package com.project.meo.serviceinterface;

import com.project.meo.entity.MeoAdmin;

public interface MeoAdminServiceInf {
	public MeoAdmin validateAdmin(String emailId,String password);
}
