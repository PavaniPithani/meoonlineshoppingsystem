package com.project.meo.serviceinterface;

import java.util.List;

import com.project.meo.dto.CustomerCartDTO;
import com.project.meo.entity.CustomerCart;
import com.project.meo.entity.CustomerOrderDetails;

public interface CustomerCartServiceInf {
	
	public void addProductToCart(CustomerCart customerCartProduct);
	public void updateCustomerCart(float subTotal,int quantity,int sindex);
	public CustomerCartDTO getProductFromCart(int productId,int customerId);
	public List<CustomerCartDTO> getAllProductsFromCart(int customerId);
	public int getCartTotalPrice(int customerId);
	public void deleteProductFromCart(int productId,int customerId);
	public List<CustomerOrderDetails> getProductsFromCustomerCart(int customerId);
	public void deleteOrderedProductFromCart(int customerId);
}
