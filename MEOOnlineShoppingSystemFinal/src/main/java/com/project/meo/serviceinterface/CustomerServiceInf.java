package com.project.meo.serviceinterface;

import com.project.meo.entity.Customer;

public interface CustomerServiceInf {
	
	public Customer saveCustomer(Customer customer);
	public Customer validateCustomer(String emailId,String password);
}
