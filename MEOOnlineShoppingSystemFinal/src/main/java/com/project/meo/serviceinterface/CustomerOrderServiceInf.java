package com.project.meo.serviceinterface;

import java.util.List;

import com.project.meo.dto.CustomerOrderDTO;
import com.project.meo.entity.CustomerOrder;

public interface CustomerOrderServiceInf {
	public CustomerOrder placeOrder(CustomerOrder customerOrder);
	public List<CustomerOrderDTO> getOrderDetails(int customerId);
}
