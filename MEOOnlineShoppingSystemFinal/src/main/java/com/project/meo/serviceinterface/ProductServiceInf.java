package com.project.meo.serviceinterface;

import java.util.List;

import com.project.meo.dto.ProductDTO;
import com.project.meo.entity.Product;

public interface ProductServiceInf {
	public void addProduct(Product product);
	public List<ProductDTO> getAllProductsByFid(int aid);
	public List<ProductDTO> getAllProducts();
	public ProductDTO getProductById(int productId);
	public void deleteProduct(int productId);
	public void updateProductStock(int stock,int productId);
	public int getProductStock(int productID);
}
