package com.project.meo.serviceinterface;

import java.util.List;

import com.project.meo.dto.CustomerOrderDetailsDTO;

public interface CustomerOrderDetailsServiceInf {
	public List<CustomerOrderDetailsDTO> getOrderedProductDetails(int customerId); 
}
