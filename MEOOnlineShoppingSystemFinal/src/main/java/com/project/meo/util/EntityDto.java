package com.project.meo.util;

import org.springframework.stereotype.Component;
import com.project.meo.dto.CustomerDTO;
import com.project.meo.dto.ProductDTO;
import com.project.meo.entity.Customer;
import com.project.meo.entity.Product;

@Component
public class EntityDto {

	public Product productDtoEntityMapping(ProductDTO productDto) {
		Product product=new Product();
		product.setProductId(productDto.getProductId());
		product.setProductCategory(productDto.getProductCategory());
		product.setProductName(productDto.getProductName());
		product.setProductPrice(productDto.getProductPrice());
		product.setStock(productDto.getStock());
		return product;
	}
	
	public Customer getCustomerDetails(CustomerDTO customerDto) {
		Customer regCustomer=new Customer();
		regCustomer.setName(customerDto.getName());
		regCustomer.setEmailId(customerDto.getEmailId());
		regCustomer.setMobileNumber(customerDto.getMobileNumber());
		regCustomer.setPassword(customerDto.getPassword());
		regCustomer.setCity(customerDto.getCity());
		regCustomer.setState(customerDto.getState());
		regCustomer.setPostalCode(customerDto.getPostalCode());
		return regCustomer;
	}
	

}
