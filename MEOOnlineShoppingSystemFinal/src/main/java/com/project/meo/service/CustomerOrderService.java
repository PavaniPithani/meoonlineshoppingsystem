package com.project.meo.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.project.meo.dto.CustomerOrderDTO;
import com.project.meo.entity.CustomerOrder;
import com.project.meo.repository.CustomerOrderRepository;
import com.project.meo.serviceinterface.CustomerOrderServiceInf;

@Service
public class CustomerOrderService implements CustomerOrderServiceInf{

	@Autowired
	CustomerOrderRepository customerOrderRepository;
	
	public CustomerOrder placeOrder(CustomerOrder customerOrder) {
		return customerOrderRepository.save(customerOrder);
	}
	
	public List<CustomerOrderDTO> getOrderDetails(int customerId) {
		return customerOrderRepository.getOrderDetails(customerId);
	}


}