package com.project.meo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.meo.dto.CustomerCartDTO;
import com.project.meo.entity.CustomerCart;
import com.project.meo.entity.CustomerOrderDetails;
import com.project.meo.repository.CustomerCartRepository;
import com.project.meo.serviceinterface.CustomerCartServiceInf;

@Service
public class CustomerCartService implements CustomerCartServiceInf{

	@Autowired
	CustomerCartRepository customerCartRepository;
	
	public void addProductToCart(CustomerCart customerCartProduct) {
		customerCartRepository.save(customerCartProduct);
	}
	
	public void updateCustomerCart(float subTotal,int quantity,int sindex) {
		customerCartRepository.updateCustomerCart(subTotal, quantity, sindex);
	}
	
	public CustomerCartDTO getProductFromCart(int productId,int customerId) {
		return customerCartRepository.getProductFromCart(productId, customerId);
	}
	
	public List<CustomerCartDTO> getAllProductsFromCart(int customerId){
		return customerCartRepository.getAllProductsFromCustomerCart(customerId);
	}
	
	public int getCartTotalPrice(int customerId) {
		return customerCartRepository.getCartTotalPrice(customerId);
	}
	
	public void deleteProductFromCart(int productId,int customerId) {
		customerCartRepository.deleteProductFromCart(productId,customerId);
	}
	
	public List<CustomerOrderDetails> getProductsFromCustomerCart(int customerId) {
		return customerCartRepository.getProductsFromCustomerCart(customerId);
	}
	
	
	public void deleteOrderedProductFromCart(int customerId) {
		customerCartRepository.deleteOrderedProductFromCart(customerId) ;
	}
}
