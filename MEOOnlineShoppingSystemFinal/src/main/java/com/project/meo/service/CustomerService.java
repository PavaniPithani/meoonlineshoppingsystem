package com.project.meo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.project.meo.entity.Customer;
import com.project.meo.repository.CustomerRepository;
import com.project.meo.serviceinterface.CustomerServiceInf;

@Service
public class CustomerService implements CustomerServiceInf{

	Customer customer;
	@Autowired
	CustomerRepository customerRepository;
	
	public Customer saveCustomer(Customer customer) {
		return customerRepository.save(customer);	
	}
	
	public Customer validateCustomer(String emailId,String password){
		customer=customerRepository.findByEmailIdAndPassword(emailId,password);
		return customer;
	}
	

}
