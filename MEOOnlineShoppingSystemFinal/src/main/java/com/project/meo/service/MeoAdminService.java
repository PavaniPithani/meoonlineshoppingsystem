package com.project.meo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.project.meo.entity.MeoAdmin;
import com.project.meo.repository.MeoAdminRepository;
import com.project.meo.serviceinterface.MeoAdminServiceInf;

@Service
public class MeoAdminService implements MeoAdminServiceInf{

	MeoAdmin meoAdmin;
	@Autowired
	MeoAdminRepository adminRepository;
	
	public MeoAdmin validateAdmin(String emailId,String password) {
		meoAdmin=adminRepository.findByEmailIdAndPassword(emailId,password);
		return meoAdmin;
	}
	


}
