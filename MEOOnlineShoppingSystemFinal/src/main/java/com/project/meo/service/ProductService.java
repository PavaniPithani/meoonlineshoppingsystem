package com.project.meo.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.project.meo.dto.ProductDTO;
import com.project.meo.entity.Product;
import com.project.meo.repository.ProductRepository;
import com.project.meo.serviceinterface.ProductServiceInf;

@Service
public class ProductService implements ProductServiceInf {

	@Autowired
	ProductRepository productRepository;
	Logger logger=LoggerFactory.getLogger(ProductService.class);
	
	public void addProduct(Product product) {
		logger.info("Adding product to DB");
		productRepository.save(product);
	}
	
	public List<ProductDTO> getAllProductsByFid(int aid) {
		logger.info("Will get list of products");
		return productRepository.getAllProductsByFid(aid);
	}
	
	public List<ProductDTO> getAllProducts() {
		return productRepository.getAllProducts();
	}
	
	public ProductDTO getProductById(int productId) {
		return productRepository.getProductById(productId);
	}

	public void deleteProduct(int productId) {
		logger.info("Product will be deleted");
		productRepository.deleteById(productId);
	}

	public void updateProductStock(int stock,int productId) {
			productRepository.updateProductStock(stock, productId);
	}
	
	public int getProductStock(int productID) {
		logger.info("will get product stock");
		return productRepository.getProductStock(productID);
	}
}
