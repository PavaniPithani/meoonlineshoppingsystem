package com.project.meo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.meo.dto.CustomerOrderDetailsDTO;
import com.project.meo.repository.CustomerOrderDetailsRepository;
import com.project.meo.serviceinterface.CustomerOrderDetailsServiceInf;

@Service
public class CustomerOrderDetailsService implements CustomerOrderDetailsServiceInf {
	
	@Autowired
	CustomerOrderDetailsRepository customerOrderDetailsRepository;

	public List<CustomerOrderDetailsDTO> getOrderedProductDetails(int customerId) {
		return customerOrderDetailsRepository.getOrderedProductDetails(customerId);
	}
}
