package com.project.meo.entity;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CustomerOrder {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int orderID;
	private float totalAmount;
	private LocalDate orderDate;
	private LocalDate expecteddeliveryDate;
	
	@ManyToOne
	private Customer customerfid;
	
	@OneToMany(targetEntity=CustomerOrderDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="orderid_fid",referencedColumnName="orderID")
	private List<CustomerOrderDetails> orderProductList;

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public LocalDate getExpecteddeliveryDate() {
		return expecteddeliveryDate;
	}

	public void setExpecteddeliveryDate(LocalDate expecteddeliveryDate) {
		this.expecteddeliveryDate = expecteddeliveryDate;
	}

	public Customer getCustomerfid() {
		return customerfid;
	}

	public void setCustomerfid(Customer customerfid) {
		this.customerfid = customerfid;
	}

	public List<CustomerOrderDetails> getOrderProductList() {
		return orderProductList;
	}

	public void setOrderProductList(List<CustomerOrderDetails> orderProductList) {
		this.orderProductList = orderProductList;
	}
	
	

}
