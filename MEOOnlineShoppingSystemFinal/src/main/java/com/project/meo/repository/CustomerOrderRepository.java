package com.project.meo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.project.meo.dto.CustomerOrderDTO;
import com.project.meo.entity.CustomerOrder;

public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, Integer> {

	
	@Query("select new com.project.meo.dto.CustomerOrderDTO(c.orderID,c.totalAmount,"
			+ "c.orderDate,c.expecteddeliveryDate) "
			+ "from CustomerOrder c  where c.customerfid.customerId=?1")
	List<CustomerOrderDTO> getOrderDetails(int customerId);

}
