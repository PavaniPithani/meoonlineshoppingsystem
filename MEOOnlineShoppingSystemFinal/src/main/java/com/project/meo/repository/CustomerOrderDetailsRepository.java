package com.project.meo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.project.meo.dto.CustomerOrderDetailsDTO;
import com.project.meo.entity.CustomerOrderDetails;

public interface CustomerOrderDetailsRepository extends JpaRepository<CustomerOrderDetails, Integer> {

	@Query("select new com.project.meo.dto.CustomerOrderDetailsDTO(c.orderID,op.productId,"
		 +"op.productCategory,op.productName,op.productPrice,op.subTotal,op.quantity) "
		+ "from CustomerOrder c JOIN c.orderProductList op where  c.customerfid.customerId=?1")
	List<CustomerOrderDetailsDTO> getOrderedProductDetails(int customerId);
	
	
}
