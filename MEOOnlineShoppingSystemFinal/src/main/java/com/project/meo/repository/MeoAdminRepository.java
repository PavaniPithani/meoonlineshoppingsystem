package com.project.meo.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.project.meo.entity.MeoAdmin;


public interface MeoAdminRepository extends JpaRepository<MeoAdmin, Integer> {

	MeoAdmin findByEmailIdAndPassword(String emailId, String password);
	

	

}
