package com.project.meo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.project.meo.dto.CustomerCartDTO;
import com.project.meo.entity.CustomerCart;
import com.project.meo.entity.CustomerOrderDetails;

public interface CustomerCartRepository extends JpaRepository<CustomerCart, Integer> {

	@Query("select new com.project.meo.dto.CustomerCartDTO(ccp.sindex,ccp.productId,ccp.productCategory,ccp.productName,"
			+ "ccp.productPrice,ccp.subTotal,ccp.quantity) "
		+ "from CustomerCart ccp where ccp.productId=?1 and ccp.customer.customerId=?2")
	CustomerCartDTO getProductFromCart(int productId,int customerId);
	
	
	@Modifying
	@Transactional
	@Query("update CustomerCart set subTotal=?1,quantity=?2 where sindex=?3")
	void updateCustomerCart(float subTotal,int quantity,int sindex);
	
	
	@Query("select new com.project.meo.dto.CustomerCartDTO(ccp.sindex,ccp.productId,ccp.productCategory,ccp.productName,"
			+ "ccp.productPrice,ccp.subTotal,ccp.quantity) "
		+ "from CustomerCart ccp where ccp.customer.customerId=?1")
	List<CustomerCartDTO> getAllProductsFromCustomerCart(int customerId);
	
	@Query("select sum(ccp.subTotal) from CustomerCart ccp where ccp.customer.customerId=?1")
	int getCartTotalPrice(int customerID);

	@Modifying
	@Transactional
	@Query("delete from CustomerCart ccp where ccp.productId=?1 and ccp.customer.customerId=?2")
	void deleteProductFromCart(int productId, int customerId);
	
	
	@Query("select new com.project.meo.entity.CustomerOrderDetails(ccp.productId,ccp.productCategory,ccp.productName,"
			+ "ccp.productPrice,ccp.subTotal,ccp.quantity) "
		+ "from CustomerCart ccp where ccp.customer.customerId=?1")
	List<CustomerOrderDetails> getProductsFromCustomerCart(int customerId);
	
	
	@Modifying
	@Transactional
	@Query("delete from CustomerCart ccp where ccp.customer.customerId=?1")
	void deleteOrderedProductFromCart(int customerId);
	
	
	
	
	
	
	
	
	
	
	
	
	
}
