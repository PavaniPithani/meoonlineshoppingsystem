package com.project.meo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.meo.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {


	Customer findByEmailIdAndPassword(String emailId, String password);
}
