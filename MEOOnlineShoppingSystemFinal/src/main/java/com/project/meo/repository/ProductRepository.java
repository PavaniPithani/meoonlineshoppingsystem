package com.project.meo.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.project.meo.dto.ProductDTO;
import com.project.meo.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

	@Query("select new com.project.meo.dto.ProductDTO(p.productId,p.productCategory,p.productName,p.productPrice,p.stock) "
			+ "from Product p where p.fid.adminId=?1")
	List<ProductDTO> getAllProductsByFid(int fid);
	
	
	@Query("select new com.project.meo.dto.ProductDTO(p.productId,p.productCategory,p.productName,p.productPrice,p.stock) "
			+ "from Product p where p.stock>=1")
	List<ProductDTO> getAllProducts();
	
	@Query("select new com.project.meo.dto.ProductDTO(p.productId,p.productCategory,p.productName,p.productPrice,p.stock) "
			+ "from Product p where p.productId=?1")
	ProductDTO getProductById(int productId);
	
	
	@Modifying
	@Transactional
	@Query("update Product set stock=?1 where productId=?2")
	void updateProductStock(int stock,int productId);

	@Query("select p.stock from Product p where p.productId=?1")
	int getProductStock(int productId);

	
}
