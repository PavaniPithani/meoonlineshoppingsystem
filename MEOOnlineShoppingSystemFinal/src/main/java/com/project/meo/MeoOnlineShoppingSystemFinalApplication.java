package com.project.meo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeoOnlineShoppingSystemFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeoOnlineShoppingSystemFinalApplication.class, args);
	}

}
