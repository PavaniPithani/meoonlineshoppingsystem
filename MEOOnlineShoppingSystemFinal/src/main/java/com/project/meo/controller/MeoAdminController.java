package com.project.meo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.project.meo.customexception.CustomException;
import com.project.meo.dto.MeoAdminDTO;
import com.project.meo.entity.MeoAdmin;
import com.project.meo.service.MeoAdminService;

/**
 * <i>MeoAdminController</i> handle the requests made by the Admin
 * */

@RestController
public class MeoAdminController {

	MeoAdmin meoAdmin;
	@Autowired
	MeoAdminService adminService;
	
	/**
	 * <i>validateAdmin</i> method accepts the credentials from the user and 
	 * interact with the AdminService class for completing the credentials validation.
	 * If the credentials are invalid, as a response the adminSignIn page will be returned.
	 * If the credentials are valid,as as response the adminPage will be returned.
	 * 
	 * */

	@PostMapping("/validateAdmin")
	public ModelAndView validateAdmin(@ModelAttribute MeoAdminDTO meoAdminDTO) {
		String emailId=meoAdminDTO.getEmailId();
		String password=meoAdminDTO.getPassword();
		ModelAndView mv=new ModelAndView();
		try {
			meoAdmin=adminService.validateAdmin(emailId, password);
			if(meoAdmin==null) {
				throw new CustomException("Invalid Credentials");
			}
		} catch (CustomException e) {
			mv.addObject("msg",e.getMessage());
			mv.setViewName("adminSignIn");
			return mv;
		}
		mv.setViewName("adminPage");
		return mv;
	}
	
	/**
	 * On Admin request this method will return adminPage
	 * */	
	@GetMapping("/adminPage")
	public ModelAndView getadminPage() {
		ModelAndView mv=new ModelAndView();
		if(meoAdmin!=null) {
			mv.setViewName("adminPage");
		}
		return mv;
	}
	
	/**
	 * This method is invoked,when admin request for adding the new product into the system.
	 * As a response,it will return adminNewProduct page in which the admin can fill the product details.
	 * */
	
	@GetMapping("/adminNewProductPage")
	public ModelAndView getadminNewProductPage() {
		ModelAndView mv=new ModelAndView();
		if(meoAdmin!=null) {
			mv.setViewName("adminNewProduct");
		}
		return mv;
	}

	@GetMapping("/adminLogOut")
	public ModelAndView adminLogOut() {
		meoAdmin=null;
		ModelAndView mv=new ModelAndView();
		mv.setViewName("meoHome");
		return mv;
	}


	
}















