package com.project.meo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.project.meo.dto.CustomerCartDTO;
import com.project.meo.dto.ProductDTO;
import com.project.meo.entity.Customer;
import com.project.meo.entity.CustomerCart;
import com.project.meo.service.CustomerCartService;
import com.project.meo.service.ProductService;

@RestController
public class CustomerCartController {

	
	Customer customer;
	@Autowired
	CustomerController customerController;
	@Autowired
	ProductService productService;
	@Autowired
	CustomerCartService customerCartService;
	
	
	@GetMapping("/addToCart")
	public ModelAndView addToCart(@RequestParam int productId) {
		int quantity=1;
		float subTotal=0;
		float productPrice=0;
		ModelAndView mv=new  ModelAndView();
		CustomerCart cartProduct=new CustomerCart();
		customer=customerController.customer;
		ProductDTO productDto=productService.getProductById(productId);
		CustomerCartDTO customerCartDTO=customerCartService.getProductFromCart(productId,customer.getCustomerId());
		productPrice=productDto.getProductPrice();
		int stock=productDto.getStock();
		cartProduct.setProductId(productDto.getProductId());
		cartProduct.setProductCategory(productDto.getProductCategory());
		cartProduct.setProductName(productDto.getProductName());
		cartProduct.setProductPrice(productPrice);
		cartProduct.setCustomer(customer);
		
		if(customerCartDTO!=null) {
			subTotal=customerCartDTO.getSubTotal()+productPrice;
			quantity=customerCartDTO.getQuantity()+1;
			int sindex=customerCartDTO.getSindex();
			customerCartService.updateCustomerCart(subTotal, quantity, sindex);
		}
		
		else {
			subTotal=productPrice;
			cartProduct.setSubTotal(subTotal);
			cartProduct.setQuantity(quantity);
			customerCartService.addProductToCart(cartProduct);
		}
		productService.updateProductStock((stock-1),productDto.getProductId());
		List<ProductDTO> productList=productService.getAllProducts();
		mv.addObject("productList", productList); 
		mv.addObject("msg",productDto.getProductName()+" added to the Cart successfully!");
		mv.setViewName("customerAllProducts");
		return mv;
	}
	
	@GetMapping("/customerCart")
	public ModelAndView getCustomerCartProducts() {
		 float cartTotalPrice=0;
		 ModelAndView mv=new ModelAndView();
		 customer=customerController.customer;
		 List<CustomerCartDTO> customerCart=customerCartService.getAllProductsFromCart(customer.getCustomerId());
		 if(!customerCart.isEmpty()) {
			 cartTotalPrice=customerCartService.getCartTotalPrice(customer.getCustomerId());
			 mv.addObject("totalPrice",cartTotalPrice);
			 mv.addObject("customerCart", customerCart);
		 }
		 else {
			 mv.addObject("msg","emptyCart");
		 }
		 mv.setViewName("customerCartView");
		 return mv;
	}
	
	@RequestMapping("/removeProductFromCart")
	public ModelAndView removeProductFromCart(@RequestParam int productId,@RequestParam int quantity) {
		ModelAndView mv=new ModelAndView();
		customer=customerController.customer;
		int stock=0;
		stock=productService.getProductStock(productId);
		stock+=quantity;
		productService.updateProductStock(stock,productId);
		customerCartService.deleteProductFromCart(productId,customer.getCustomerId());
		 List<CustomerCartDTO> customerCart=customerCartService.getAllProductsFromCart(customer.getCustomerId());
		 if(!customerCart.isEmpty()) {
			 int cartTotalPrice=customerCartService.getCartTotalPrice(customer.getCustomerId());
			 mv.addObject("totalPrice",cartTotalPrice);
			 mv.addObject("customerCart", customerCart);
		 }
		 else {
			 mv.addObject("msg","emptyCart");
		 }
		 mv.setViewName("customerCartView");
		return mv;
	}
}
