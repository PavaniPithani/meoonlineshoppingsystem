package com.project.meo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.project.meo.dto.ProductDTO;
import com.project.meo.entity.Customer;
import com.project.meo.entity.MeoAdmin;
import com.project.meo.entity.Product;
import com.project.meo.service.ProductService;
import com.project.meo.util.EntityDto;

@RestController
public class ProductController {
	
	MeoAdmin meoAdmin;
	@Autowired
	MeoAdminController meoAdminController;
	Customer customer;
	@Autowired
	CustomerController customerController;
	@Autowired
	EntityDto entityDto;
	@Autowired
	ProductService productService;
	
	public static final String PRODUCTLIST="productList";
	public static final String ADMINALLPRODUCTSVIEW="adminAllProducts";
	
	@PostMapping("/addProduct")
	public ModelAndView addProduct(@ModelAttribute ProductDTO productDto) {
		ModelAndView mv=new ModelAndView();
		Product product=entityDto.productDtoEntityMapping(productDto);
		meoAdmin=meoAdminController.meoAdmin;
		if(meoAdmin!=null) {
			product.setFid(meoAdmin);
			productService.addProduct(product);
		}
		mv.addObject("msg","Product added successfully!");
		mv.setViewName("adminNewProduct");
		return mv;
	}
	
	@GetMapping("/AdminAllProducts")
	public ModelAndView getAdminAllProducts() {
		ModelAndView mv=new ModelAndView();
		meoAdmin=meoAdminController.meoAdmin;
		if(meoAdmin!=null) {
			List<ProductDTO> pList=productService.getAllProductsByFid(meoAdmin.getAdminId());
			mv.addObject(PRODUCTLIST,pList);
		}
		mv.setViewName(ADMINALLPRODUCTSVIEW);
		return mv;
	}
	
	@GetMapping("/getProduct")
	public ModelAndView getProduct(@RequestParam int productId) {
		ProductDTO product=productService.getProductById(productId);
		ModelAndView mv=new ModelAndView();
		mv.addObject("product",product);
		mv.setViewName("adminEditProduct");
		return mv;
	}
	
	@RequestMapping("/updateProduct")
	public ModelAndView updateProduct(@ModelAttribute ProductDTO productDto) {
		Product product=entityDto.productDtoEntityMapping(productDto);
		meoAdmin=meoAdminController.meoAdmin;
		product.setFid(meoAdmin);
		ModelAndView mv=new ModelAndView();
		if(meoAdmin!=null) {
			productService.addProduct(product);
			List<ProductDTO> pList=productService.getAllProductsByFid(meoAdmin.getAdminId());
			mv.addObject(PRODUCTLIST,pList);
		}
		mv.addObject("msg","Product updated successfully!");
		mv.setViewName(ADMINALLPRODUCTSVIEW);
		return mv;
	}
	
	
	@RequestMapping("/deleteProduct")
	public ModelAndView deleteProduct(@RequestParam int productId) {
		productService.deleteProduct(productId);
		meoAdmin=meoAdminController.meoAdmin;
		List<ProductDTO> pList=productService.getAllProductsByFid(meoAdmin.getAdminId());
		ModelAndView mv=new ModelAndView();
		mv.addObject(PRODUCTLIST,pList);
		mv.addObject("msg","Product deleted successfully!");
		mv.setViewName(ADMINALLPRODUCTSVIEW);
		return mv;
	}
	
	
	@GetMapping("/allProducts")
	public ModelAndView getAllProducts() {
		List<ProductDTO> productList=productService.getAllProducts();
		ModelAndView mv=new ModelAndView();
		customer=customerController.customer;
		if(customer!=null) {
			mv.addObject(PRODUCTLIST, productList); 
			mv.setViewName("customerAllProducts");
		}
		return mv;
	}
	
}
