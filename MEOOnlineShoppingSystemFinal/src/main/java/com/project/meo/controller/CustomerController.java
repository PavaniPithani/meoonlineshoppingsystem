package com.project.meo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.project.meo.customexception.CustomException;
import com.project.meo.dto.CustomerDTO;
import com.project.meo.entity.Customer;
import com.project.meo.service.CustomerService;
import com.project.meo.util.EntityDto;

@RestController
public class CustomerController {

	Customer customer;
	@Autowired
	CustomerService customerService;	
	@Autowired
	EntityDto entityDto;

	@PostMapping("/saveCustomer")
	public ModelAndView saveCustomer(@ModelAttribute CustomerDTO customerDto) {
		Customer regCustomer=entityDto.getCustomerDetails(customerDto);
		customerService.saveCustomer(regCustomer);
		ModelAndView mv=new ModelAndView();
		mv.setViewName("customerSignIn");
		return mv;
	}
	
	
	@PostMapping("/validateCustomer")
	public ModelAndView validateCustomer(@ModelAttribute CustomerDTO customerDto){
		String emailId=customerDto.getEmailId();
		String password=customerDto.getPassword();
		ModelAndView mv=new ModelAndView();
		try {
			customer=customerService.validateCustomer(emailId, password);
			if(customer==null) {
				throw new CustomException("Invalid Credentials");
			}
		} catch (CustomException e) {
			mv.addObject("msg",e.getMessage());
			mv.setViewName("customerSignIn");
			return mv; 
		}
		mv.setViewName("customerPage");
		return mv;
	}
	
	@GetMapping("/customerPage")
	public ModelAndView getCustomerPage() {
		ModelAndView mv=new ModelAndView();
		if(customer!=null) {
			mv.setViewName("customerPage");
		}
		return mv;
	}
	
	
	@GetMapping("/customerLogOut")
	public ModelAndView customerLogOut() {
		customer=null;
		ModelAndView mv=new ModelAndView();
		mv.setViewName("meoHome");
		return mv;
	}

	
	
	
	
	
	
	
	
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
}
