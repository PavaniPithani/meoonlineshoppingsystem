package com.project.meo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class HomeController {

	@GetMapping("/")
	public ModelAndView getHome() {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("meoHome");
		return mv;
	}
	
	
	@GetMapping("/adminSignIn")
	public ModelAndView adminSignIn() {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("adminSignIn");
		return mv;
	}
	
	@GetMapping("/customerSignUp")
	public ModelAndView customerSignUp() {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("customerSignUp");
		return mv;
	}
	
	@GetMapping("/customerSignIn")
	public ModelAndView customerSignIn() {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("customerSignIn");
		return mv;
	}
	
}
