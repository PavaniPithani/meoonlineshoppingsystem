package com.project.meo.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.project.meo.dto.CustomerDTO;
import com.project.meo.dto.CustomerOrderDTO;
import com.project.meo.dto.CustomerOrderDetailsDTO;
import com.project.meo.entity.Customer;
import com.project.meo.entity.CustomerOrder;
import com.project.meo.entity.CustomerOrderDetails;
import com.project.meo.service.CustomerCartService;
import com.project.meo.service.CustomerOrderDetailsService;
import com.project.meo.service.CustomerOrderService;

@RestController
public class CustomerOrderController {
	
	Customer customer;
	@Autowired
	CustomerController customerController;
	@Autowired
	CustomerCartService customerCartService;
	@Autowired
	CustomerOrderService customerOrderService;
	@Autowired
	CustomerOrderDetailsService customerOrderDetailsService;
	
	@GetMapping("/placeOrder")
	public ModelAndView placeOrder() {
		customer=customerController.customer;
		float totalAmount=customerCartService.getCartTotalPrice(customer.getCustomerId());
		ModelAndView mv=new ModelAndView();
		CustomerOrder customerOrder=new CustomerOrder();
		List<CustomerOrderDetails> cartProductList=customerCartService.getProductsFromCustomerCart(customer.getCustomerId());
		LocalDate orderDate=LocalDate.now();
		LocalDate expecteddeliveryDate=orderDate.plusDays(3);
		customerOrder.setTotalAmount(totalAmount);
		customerOrder.setOrderDate(orderDate);
		customerOrder.setExpecteddeliveryDate(expecteddeliveryDate);
		customerOrder.setCustomerfid(customer);
		customerOrder.setOrderProductList(cartProductList);
		customerOrderService.placeOrder(customerOrder);
		customerCartService.deleteOrderedProductFromCart(customer.getCustomerId());
		mv.addObject("msg","Order Placed successfully!");
		mv.setViewName("customerOrderSuccess");
		return mv;
	}
	
	@GetMapping("/customerOrders")
	public ModelAndView  getOrderDetails() {
		customer=customerController.customer;
		ModelAndView mv=new ModelAndView();
		List<CustomerOrderDTO> orders=customerOrderService.getOrderDetails(customer.getCustomerId());
		if(!orders.isEmpty()) {
			CustomerDTO customerDto=new CustomerDTO(customer.getName(),customer.getMobileNumber(),customer.getCity(),customer.getState(),customer.getPostalCode());
			List<CustomerOrderDetailsDTO> orderProducts=customerOrderDetailsService.getOrderedProductDetails(customer.getCustomerId());
			mv.addObject("orders",orders);
			mv.addObject("orderProducts",orderProducts);
			mv.addObject("customerDetails",customerDto);
		}
		else {
			mv.addObject("msg","noOrders");
		}
		mv.setViewName("customerOrderDetails");
		return mv;
	}
	
	
}
