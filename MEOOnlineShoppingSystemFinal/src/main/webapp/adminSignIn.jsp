<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="eng">
<head>
<meta charset="ISO-8859-1">
    <title>Sign In</title>
    <link href="css/adminSignIn.css" rel="stylesheet" type="text/css">
    <script>


    function validateEmail(){
        const adminmail = document.getElementById('emailLabel');
        adminmailValue=adminmail.value.trim();
        
    	if(adminmailValue=== '') {
    		setErrorFor(adminmail, 'Email id required');
    	}	
        else if (!isValidEmail(adminmailValue)) {
    		setErrorFor(adminmail, 'Invalid email');
    	} else {
    		setSuccessFor(adminmail);
    	}
    }

    function isValidEmail(emailValue) {
    	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(emailValue);
    }



    function validatePassword(){
        const adminpassword = document.getElementById('passwordLabel');
        adminpasswordValue=adminpassword.value.trim();
        
    	if(adminpasswordValue=== '') {
    		setErrorFor(adminname, 'password required');
    	} 
        else {
    		setSuccessFor(adminpassword);
    	}
        
    }

    function setErrorFor(input, message) {
    	const formControl = input.parentElement;
    	const small = formControl.querySelector('small');
    	formControl.className = 'form-control error';
    	small.innerText = message;
    }

    function setSuccessFor(input) {
    	const formControl = input.parentElement;
    	formControl.className = 'form-control success';
    }
    
    
    </script>
</head>

<body>
    <div id="header">
        <div id="logo">MEO</div>
        <div>
            <ul id="list">
                <li><a href="/">Home</a></li>
            </ul>
        </div>
        <div id="clear"></div>
    </div>
    <h1 id="invalidc"> ${msg }</h1>
    <div id="boxmodel">
        <div id="heading">Sign In</div>
        <div class="formdiv">
            <form action="/validateAdmin" method="post" id="form" class="form">

                <div class="form-control">
                    <label for="emailLabel">Email </label>
                    <input type="email" placeholder="example@gmial.com" required id="emailLabel"
                    name="emailId" onfocusout="validateEmail()">
                    <small>Error message</small>
                </div>

                <div class="form-control">
                    <label for="passwordLabel">Password</label>
                    <input type="password" placeholder="enter your password" required id="passwordLabel"
                    name="password" onfocusout="validatePassword()">
                    <small>Error message</small>
                </div>

				<input id="submitstyle" type="submit" value="Sign In" />
                                    
            </form>
        </div>
        
    </div>

</body>
</html>