<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="eng">
<head>
<meta charset="ISO-8859-1">
<title>Customer</title>
    <link href="css/customerPage.css" rel="stylesheet" type="text/css">
    <script>
	function confirmLogOut(){
			if(confirm('Are you sure you want to logOut')){
				return true;
			}
			else{
				event.stopPropagation();
				event.preventDefault();
			}
	}
</script>
</head>

<body>
    <div id="header">
        <div id="logo">MEO</div>
        <div>
            <ul id="list">
                <li><a href="/">Home</a></li>
                <li><a href="/allProducts">Products</a></li>
                <li><a href="/customerCart">My Cart</a></li>
                <li><a href="/customerOrders">My Orders</a></li>
                <li><a href="/customerLogOut" onclick="confirmLogOut()">LogOut</a></li>
                
            </ul>
        </div>
        <div id="clear"></div>
    </div>
</body>
</html>