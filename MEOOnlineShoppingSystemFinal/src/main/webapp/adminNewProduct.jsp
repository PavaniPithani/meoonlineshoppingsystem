<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="eng">
<head>
<%@include file="adminHeader.jsp" %>
<meta charset="ISO-8859-1">
<title>New Product</title>
<link href="css/adminNewProduct.css" rel="stylesheet" type="text/css">
</head>
<body>
<h1 class="msg">${msg}</h1>
        <div id="boxmodel">
        <div id="heading">Product Details</div>
        <div class="formdiv">
            <form action="/addProduct" method="post" id="form" class="form">
                <div class="form-control">
                    <label for="categoryLabel">Product Category </label><br>
                    
                    <select name="productCategory" id="categoryLabel">
                    	<option value="electronics">Electronics</option>
                    	<option value="cosmotics">Cosmetics</option>
                    	<option value="books">Books</option>
                    	<option value="women's fashion">Women's fashion</option>
                    	<option value="men's fashion">Men's fashion</option>
                    	<option value="decoration">Decoration</option>
                    	<option value="groceries">Groceries</option>
                    </select>
      
                </div>
                
                <div class="form-control">
                    <label for="nameLabel">Product Name</label>
                    <input type="text" placeholder="enter product name" required id="nameLabel"
                    name="productName">
                </div>
                
               <div class="form-control">
                    <label for="priceLabel">Product Price</label>
                    <input type="text" placeholder="enter product price in rupees" 
                    required id="priceLabel" name="productPrice" >
                    
                </div>
                
                
                <div class="form-control">
                    <label for="stockLabel">Products in stock</label>
                    <input type="number" placeholder="enter number of available products" required id="stockLabel"
                    name="stock" >
                </div>



				<input id="submitstyle" type="submit" value="Save" />
                                    
            </form>
        </div>
        <h1></h1>
    </div>
    
</body>
</html>