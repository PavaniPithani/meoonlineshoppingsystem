<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="eng">
<head>
<%@include file="adminHeader.jsp" %>
<meta charset="ISO-8859-1">
<title>Products</title>
<link href="css/adminAllProducts.css" rel="stylesheet" type="text/css">
</head>
<script type="text/javascript">

	function confirmDelete(){
		if(confirm('Are you sure you want to delete this product')){
			return true;
		}
		else{
			event.stopPropagation();
			event.preventDefault();
		}
		
	}
</script>
<body>
<h1 class="msg">${msg}</h1>
	<div>
	 <table  class="tablecs">
 	 	<caption></caption>
		<thead>
			<tr>
				<th>Product Category</th>
				<th>Product Name</th>
				<th>Product Price</th>
				<th>Stock</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${productList}" var="product">
				<tr>
					<td><c:out value=" ${product.productCategory}"></c:out></td>
					<td><c:out value=" ${product.productName }"></c:out></td>
					<td><c:out value=" ${product.productPrice }"></c:out></td>
					<td><c:out value=" ${product.stock }"></c:out></td>
					
					<td><a href="/getProduct?productId=${product.productId }" 
						class="edit" >Update</a>
						
						<a href="/deleteProduct?productId=${product.productId }"
						onclick="confirmDelete()" class="delete">Delete</a>												
					</td>
				</tr>
			</c:forEach>
		</tbody>
	<tbody>
	</tbody>
 </table>
	</div>
</body>
</html>