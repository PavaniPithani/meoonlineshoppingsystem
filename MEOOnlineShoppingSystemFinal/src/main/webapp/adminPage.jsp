<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     
<!DOCTYPE html>
<html lang="eng">
<head>
<meta charset="ISO-8859-1">
<title>AdminHome Page</title>
<link href="css/adminPage.css" rel="stylesheet" type="text/css">
<script>
	function confirmLogOut(){
			if(confirm('Are you sure you want to logOut')){
				return true;
			}
			else{
				event.stopPropagation();
				event.preventDefault();
			}
	}
</script>
</head>
<body>
    <div id="header">
        <div id="logo">MEO</div>
        <div>
            <ul id="list">
                
                <li><a href="/">Home</a></li>
                <li><a href="/adminNewProductPage">Add New Product</a></li>
                <li><a href="/AdminAllProducts">All Products</a></li>
                <li><a href="/adminLogOut" onclick="confirmLogOut()">LogOut</a></li>
            </ul>
        </div>
        <div id="clear"></div>
    </div>
    
</body>
</html>