<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="eng">
<head>
<%@include file="customerHeader.jsp"%>
<meta charset="ISO-8859-1">
<title>Order</title>
<link href="css/customerOrderSuccess.css" rel="stylesheet" type="text/css">
</head>
<body>
<h1 class="orderSuccessText"> ${ msg }</h1>
</body>
</html>