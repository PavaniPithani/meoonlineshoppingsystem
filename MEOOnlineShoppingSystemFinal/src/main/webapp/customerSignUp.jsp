<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="eng">
<head>
	<meta charset="ISO-8859-1">
    <title>Customer Sign Up</title>
    <link href="css/customerSignup.css" rel="stylesheet" type="text/css">
    <script>
    function validateName(){
        const username = document.getElementById('nameLabel');
        usernameValue=username.value.trim();
        
    	if(usernameValue=== '') {
    		setErrorFor(username, 'Your name required ');
    	} else {
    		setSuccessFor(username);
    	}
        
    }

    function validateEmail(){
        const usermail = document.getElementById('emailLabel');
        usermailValue=usermail.value.trim();
        
    	if(usermailValue=== '') {
    		setErrorFor(usermail, 'Email id required');
    	}	
        else if (!isValidEmail(usermailValue)) {
    		setErrorFor(usermail, 'Invalid email');
    	} 
        else {
    		setSuccessFor(usermail);
    	}
    }

    function isValidEmail(emailValue) {
    	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(emailValue);
    }



    function validateMobile(){
        const usermobile = document.getElementById('mobileLabel');
        usermobileValue=usermobile .value.trim();
        
    	if(usermobileValue=== '') {
    		setErrorFor(usermobile, 'Mobile number required');
    	} 

        else if(usermobileValue.length!=10) {
    		setErrorFor(usermobile, 'Invalid mobile number');
    	}
        else {
    		setSuccessFor(usermobile);
    	}
        
    }





    function validatePassword(){
        const userpassword = document.getElementById('passwordLabel');
        userpasswordValue=userpassword.value;
        
    	if(userpasswordValue.length==0) {
    		setErrorFor(userpassword, 'Password required');
    	} 

        else if(userpasswordValue.length<6) {
    		setErrorFor(userpassword, 'Please enter atleast 6 characters');
    	}
        else {
    		setSuccessFor(userpassword);
    	}
        
    }

    function setErrorFor(input, message) {
    	const formControl = input.parentElement;
    	const small = formControl.querySelector('small');
    	formControl.className = 'form-control error';
    	small.innerText = message;
    }

    function setSuccessFor(input) {
    	const formControl = input.parentElement;
    	formControl.className = 'form-control success';
    }
    
    
    </script>
</head>

<body>
    <div id="header">
        <div id="logo">MEO</div>
        <div>
            <ul id="list">
                <li><a href="/">Home</a></li>
                <li><a href="/customerSignIn">Sign in</a></li>

            </ul>
        </div>
        <div id="clear"></div>
    </div>
    <div id="boxmodel">
        <div id="heading">Sign up</div>
        <div class="formdiv">
            <form action="/saveCustomer" method="post" id="form" class="form">
                <div class="form-control">
                    <label for="nameLabel">Your name </label>
                    <input type="text" placeholder="enter your name" required id="nameLabel" 
                    name="name"  onfocusout="validateName()">
                    <small>Error message</small>
                </div>
                <div class="form-control">
                    <label for="emailLabel">Email </label>
                    <input type="email" placeholder="example@gmial.com" required id="emailLabel"
                    name="emailId" onfocusout="validateEmail()">
                    <small>Error message</small>
                </div>
                <div class="form-control">
                    <label for="mobileLabel">Mobile number </label>
                    <input type="text" placeholder="enter your 10 digits mobile number" required id="mobileLabel"
                    name="mobileNumber" onfocusout="validateMobile()">
                    <small>Error message</small>
                </div>

                <div class="form-control">
                    <label for="passwordLabel">Password</label>
                    <input type="password" placeholder="enter atleast 6 characters" required id="passwordLabel"
                    name="password" onfocusout="validatePassword()">
                    <small>Error message</small>
                </div>

                <label id="address">Address:</label>

                <div class="form-control">
                    <label for="city">City</label>
                    <input type="text" placeholder="enter city name" required id="city"
                    name="city" onfocusout="validate()">
                    <small>Error message</small>
                </div>




                <div class="form-control">
                    <label for="state">State</label>
                    <input type="text" placeholder="enter your state" required id="state"
                    name="state" onfocusout="validate()">
                    <small>Error message</small>
                </div>


                <div class="form-control">
                    <label for="postalCode">Postal code</label>
                    <input type="text" placeholder="enter your postal code" required id="postalCode"
                    name="postalCode" onfocusout="validate()">
                    <small>Error message</small>
                </div>
	
				<input id="submitstyle" type="submit" value="Sign up" />
                                    
            </form>
        </div>
    </div>

</body>

</html>