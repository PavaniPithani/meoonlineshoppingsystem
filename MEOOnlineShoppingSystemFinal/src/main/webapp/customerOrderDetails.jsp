<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="eng">
<head>
<%@include file="customerHeader.jsp"%>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="css/customerOrderDetails.css" rel="stylesheet"
	type="text/css">
</head>
<body>
	<c:choose>
		<c:when test="${msg=='noOrders'}">
			<h1 class="emptymsg">You have not placed any orders!</h1>
		</c:when>

		<c:otherwise>
	<c:forEach items="${ orders}" var="order">
		<div id="order">
			<table class="tableorder">
			<caption >Order Details</caption>
			<thead>
				<tr>
					<th></th>
				</tr>
			</thead>
				<tbody>
					<tr>
						<td>Order Id</td>
						<td><c:out value="${order.orderID }"></c:out></td>
					</tr>


					<tr>
						<td>Total Amount</td>
						<td><c:out value="Rs ${ order.totalAmount}"></c:out></td>
					</tr>

					<tr>
						<td>Order Date</td>
						<td><c:out value="${ order.orderDate}"></c:out></td>
					</tr>
					<tr>
						<td>Expected Delivery Date</td>
						<td><c:out value="${ order.expecteddeliveryDate}"></c:out></td>
					</tr>

					<tr>
						<td id="shipad" colspan="2">Shipping Address</td>
					</tr>

					<tr>
						<td>Customer name</td>
						<td style="font-weight: 400">${customerDetails.name }</td>
					</tr>
					<tr>
						<td>Mobile Number</td>
						<td style="font-weight: 400">${customerDetails.mobileNumber}</td>
					</tr>

					<tr>
						<td>City</td>
						<td style="font-weight: 400">${customerDetails.city}</td>
					</tr>

					<tr>
						<td>State</td>
						<td style="font-weight: 400">${customerDetails.state}</td>
					</tr>

					<tr>
						<td>Postal Code</td>
						<td style="font-weight: 400">${customerDetails.postalCode}</td>
					</tr>
				</tbody>
			</table>
		</div>

		<table class="tablecs">
			<caption></caption>
			<thead>
				<tr>
					<th>Product Id</th>
					<th>Product Category</th>
					<th>Product Name</th>
					<th>Product Price</th>
					<th>Quantity</th>
					<th>Sub Total</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${orderProducts }" var="product">
					<c:if test="${ order.orderID ==product.orderID}">
						<tr>
							<td><c:out value=" ${product.productId}"></c:out></td>
							<td><c:out value=" ${product.productCategory}"></c:out></td>
							<td><c:out value=" ${product.productName }"></c:out></td>
							<td><c:out value=" ${product.productPrice }"></c:out></td>
							<td><c:out value=" ${product.quantity }"></c:out></td>
							<td><c:out value=" Rs ${product.subTotal}"></c:out></td>
						</tr>
					</c:if>
				</c:forEach>
			</tbody>
		</table>
	</c:forEach>		
		</c:otherwise>
	</c:choose>


</body>
</html>