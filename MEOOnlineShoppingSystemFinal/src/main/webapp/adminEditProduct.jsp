<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="eng">
<head>
<%@include file="adminHeader.jsp"%>
<meta charset="ISO-8859-1">
<title>Edit Product</title>
<link href="css/adminEditProduct.css" type="text/css" rel="stylesheet">
</head>
<body>
	<c:set var="selectOption" value="${product.productCategory }"></c:set>

	<div id="boxmodel">
		<div id="heading">Product Details</div>
		<div class="formdiv">
		
			<form action="/updateProduct"  id="form" class="form">
				<input type="hidden" name="method" value="PUT"/>
				<div class="form-control">
					<label for="categoryLabel">Product Category </label><br> <select
						name="productCategory" id="categoryLabel">

						<option value="electronics"
							${(selectOption=='electronics')?'selected':''}>
							Electronics</option>


						<option value="cosmotics"
							${(selectOption=='cosmotics')?'selected':''}>Cosmetics</option>

						<option value="books" ${(selectOption=='books')?'selected':''}>
							Books</option>

						<option value="women's fashion"
							${(selectOption=="women's fashion")?'selected':''}>
							Women's fashion</option>

						<option value="men's fashion"
							${(selectOption=="men's fashion")?'selected':''}>Men's
							fashion</option>

						<option value="decoration"
							${(selectOption=='decoration')?'selected':''}>
							Decoration</option>

						<option value="groceries"
							${(selectOption=='groceries')?'selected':''}>Groceries</option>
					</select>

				</div>
				
				<input hidden=true type="text" value="${ product.productId}" name="productId"/>
				<div class="form-control">
					<label for="nameLabel">Product Name</label> <input type="text"
						placeholder="enter product name" required id="nameLabel"
						name="productName" value="${product.productName }">
				</div>

				<div class="form-control">
					<label for="priceLabel">Product Price</label> <input type="text"
						placeholder="enter product price in rupees" required id="priceLabel"
						name="productPrice" value="${product.productPrice }">

				</div>


				<div class="form-control">
					<label for="stockLabel">Products in stock</label> <input
						type="number" placeholder="enter number of available products" 
						 required id="stockLabel" name="stock" value="${product.stock }">
				</div>

				<input id="submitstyle" type="submit" value="Save Changes" />

			</form>
		</div>
		<h1></h1>
	</div>

</body>
</html>