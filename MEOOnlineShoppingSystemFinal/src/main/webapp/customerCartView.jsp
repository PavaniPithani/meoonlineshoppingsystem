<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="eng">
<head>
<%@include file="customerHeader.jsp"%>
<meta charset="ISO-8859-1">
<title>Customer Cart</title>
<link href="css/customerCartView.css" rel="stylesheet" type="text/css">
<script>
	function confirmRemove(){
		if(confirm("Are you sure you want to remove the product from the cart")){
			return true;
		}
		else{
			event.stopPropagation();
			event.preventDefault();
		}
	}
	
	function confirmOrder(){
		if(confirm("Are you sure you want to place the order ")){
			return true;
		}
		else{
			event.stopPropagation();
			event.preventDefault();
		}
	}
</script>
</head>
<body>

	<c:choose>
		<c:when test="${msg=='emptyCart'}">
			<h1 class="emptymsg">Your Cart is Empty!</h1>
		</c:when>

		<c:otherwise>

			<div>
				<div id="order">
					<c:set var="tprice" value=" ${totalPrice}" />
					<c:set var="sno" value="${0 }"/>
					<c:if test="${tprice!='' }">
						<h1>Total Amount : ${totalPrice}</h1>
						<a href="/placeOrder" onclick="confirmOrder()" class="ptoOrder"><button>Proceed to Order</button></a>
					</c:if>

				</div>
				<div id="clear"></div>
				<table class="tablecs">
					<caption>Products in Cart</caption>
					<thead>
						<tr>
							<th>S.NO</th>
							<th>Product Category</th>
							<th>Product Name</th>
							<th>Product Price</th>
							<th>Quantity</th>
							<th>Sub Total</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${customerCart}" var="product">
							<tr>
								<td><c:out value=" ${sno+1}"></c:out></td>
								<c:set var="sno" value="${sno+1 }"/>
								<td><c:out value=" ${product.productCategory}"></c:out></td>
								<td><c:out value=" ${product.productName }"></c:out></td>
								<td><c:out value=" ${product.productPrice }"></c:out></td>
								<td><c:out value=" ${product.quantity }"></c:out></td>
								<td><c:out value=" Rs ${product.subTotal}"></c:out></td>
								<td><a href="/removeProductFromCart?productId=${product.productId}&
								quantity=${product.quantity }" 
									class="remove" onclick="confirmRemove()">Remove</a></td>
							</tr>
						</c:forEach>
					</tbody>
					<tbody>
					</tbody>
				</table>
			</div>
		</c:otherwise>

	</c:choose>


</body>
</html>