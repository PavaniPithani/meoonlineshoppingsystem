<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="ISO-8859-1">
    <title>Home page</title>
    <link href="css/meoHome.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="header">
        <div id="logo">MEO</div>
        <div>
            <ul id="list">
                <li><a href="/adminSignIn">Admin SignIn</a></li>
                <li><a href="/customerSignUp">Customer SignUp</a></li>
                <li><a href="/customerSignIn">Customer SignIn</a></li>
            </ul>
        </div>
        <div id="clear"></div>
    </div>

</body>
</html>


