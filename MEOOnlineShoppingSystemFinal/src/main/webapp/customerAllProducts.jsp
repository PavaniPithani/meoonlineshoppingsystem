<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="eng">
<head>
<%@include file="customerHeader.jsp"%>
<meta charset="ISO-8859-1">
<title>All Products</title>
<link href="css/customerAllProducts.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div>
	<h1 class="msg">${msg}</h1>
	 <table  class="tablecs">
 	 	<caption></caption>
		<thead>
			<tr>
				<th>Product Category</th>
				<th>Product Name</th>
				<th>Product Price</th>
				<th>Stock</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${productList}" var="product">
				<tr>
					<td><c:out value=" ${product.productCategory}"></c:out></td>
					<td><c:out value=" ${product.productName }"></c:out></td>
					<td><c:out value=" Rs ${product.productPrice }"></c:out></td>
					<td><c:out value=" ${product.stock }"></c:out></td>
					
					<td><a href="/addToCart?productId=${product.productId }" 
						class="cart" >Add to Cart</a>
																	
					</td>
				</tr>
			</c:forEach>
		</tbody>
	<tbody>
	</tbody>
 </table>
</div>
</body>
</html>