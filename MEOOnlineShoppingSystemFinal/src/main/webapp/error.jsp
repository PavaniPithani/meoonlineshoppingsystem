<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="eng">
<head>
<meta charset="ISO-8859-1">
<title>Error</title>
<link href="css/error.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="errorText">
	<h1>Something went wrong!</h1>
	<h1>Please try again!</h1>
	<a href="/"><button class="buttonstyle" >Okay</button></a>
</div>

</body>
</html>