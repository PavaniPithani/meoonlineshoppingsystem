package com.project.meo.servicetest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.meo.entity.Customer;
import com.project.meo.repository.CustomerRepository;
import com.project.meo.service.CustomerService;

@RunWith(SpringRunner.class)
@SpringBootTest
class CustomerServiceTest {

	@Autowired
	CustomerService customerService;
	
	@MockBean
	CustomerRepository customerRepository;
	
	@Test
	void test() {
		Customer customer=new Customer(1,"pavani","pavani@gmail.com",9876543212l,
				"123456","kakinada","Andhra",433234,null,null);
		when(customerRepository.save(customer)).thenReturn(customer);
		assertEquals(customer,customerService.saveCustomer(customer));
	}
	
	@Test
	void validateCustomerTest() {
		String emailId="pavani@gmail.com";
		String password="123456";
		Customer customer=new Customer(1,"pavani","pavani@gmail.com",9876543212l,
				"123456","kakinada","Andhra",433234,null,null);
		when(customerRepository.findByEmailIdAndPassword(emailId,password)).thenReturn(customer);
		assertEquals(customer,customerService.validateCustomer(emailId,password));
	}

}
