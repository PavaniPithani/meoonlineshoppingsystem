package com.project.meo.servicetest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.project.meo.dto.CustomerOrderDetailsDTO;
import com.project.meo.repository.CustomerOrderDetailsRepository;
import com.project.meo.service.CustomerOrderDetailsService;

@RunWith(SpringRunner.class)
@SpringBootTest
class CustomerOrderDetailsServiceTest {

	@Autowired
	CustomerOrderDetailsService customerOrderDetailsService;
	
	@MockBean
	CustomerOrderDetailsRepository customerOrderDetailsRepository;
	
	@Test
	void getOrderedProductDetailsTest() {
		int customerId=1;
		when(customerOrderDetailsRepository.getOrderedProductDetails(customerId))
		.thenReturn(Stream.of(new CustomerOrderDetailsDTO(1,1,"Books","Our Earth",123,123,1), new CustomerOrderDetailsDTO(2,1,"Books","Our Earth",123,123,1)).collect(Collectors.toList()));
        assertEquals(2, customerOrderDetailsService. getOrderedProductDetails(customerId).size());
	}

}

