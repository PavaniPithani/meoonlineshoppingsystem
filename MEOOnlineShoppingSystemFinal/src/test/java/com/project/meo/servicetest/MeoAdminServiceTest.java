package com.project.meo.servicetest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.meo.entity.MeoAdmin;
import com.project.meo.repository.MeoAdminRepository;
import com.project.meo.service.MeoAdminService;

@RunWith(SpringRunner.class)
@SpringBootTest
class MeoAdminServiceTest {

	@Autowired
	MeoAdminService meoAdminService;
	@MockBean
	MeoAdminRepository adminRepository;
	
	@Test
	void validateAdminTest() {
		String emailId="pavani@gmail.com";
		String password="123456";
		MeoAdmin meoAdmin=new MeoAdmin(1,"Pavani","pavani@gmail.com","123456",null);
		when(adminRepository.findByEmailIdAndPassword(emailId,password)).thenReturn(meoAdmin);
		assertEquals(meoAdmin,meoAdminService.validateAdmin(emailId,password));
	}

}
