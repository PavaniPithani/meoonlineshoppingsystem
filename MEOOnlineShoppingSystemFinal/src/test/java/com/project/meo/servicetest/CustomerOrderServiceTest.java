package com.project.meo.servicetest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.project.meo.dto.CustomerOrderDTO;
import com.project.meo.entity.CustomerOrder;
import com.project.meo.repository.CustomerOrderRepository;
import com.project.meo.service.CustomerOrderService;

@RunWith(SpringRunner.class)
@SpringBootTest
class CustomerOrderServiceTest {

	@Autowired
	CustomerOrderService customerOrderService;
	@MockBean
	CustomerOrderRepository customerOrderRepository;
	
	@Test
	void placeOrderTest() {
		CustomerOrder customerOrder=new CustomerOrder(1,200,null,null,null,null);
		when(customerOrderRepository.save(customerOrder)).thenReturn(customerOrder);
		assertEquals(customerOrder,customerOrderService.placeOrder(customerOrder));
		
	}

	@Test
	void getOrderDetails() {
		int customerID=1;
        when(customerOrderRepository.getOrderDetails(customerID)).thenReturn(Stream
                .of(new CustomerOrderDTO(1,200,null,null), new CustomerOrderDTO(1,200,null,null)).collect(Collectors.toList()));
        assertEquals(2, customerOrderService.getOrderDetails(customerID).size());
	}
}









