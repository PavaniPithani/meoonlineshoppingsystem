package com.project.meo.servicetest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.project.meo.dto.CustomerCartDTO;
import com.project.meo.entity.CustomerCart;
import com.project.meo.entity.CustomerOrderDetails;
import com.project.meo.repository.CustomerCartRepository;
import com.project.meo.service.CustomerCartService;

@RunWith(SpringRunner.class)
@SpringBootTest
class CustomerCartServiceTest {

	@Autowired
	CustomerCartService customerCartService;
	
	@MockBean
	CustomerCartRepository customerCartRepository;
	
	@Test
	void addProductToCartTest() {
		CustomerCart customerCart=new CustomerCart(1,1,"Books","Our Earth",123,123,1,null);
		customerCartService.addProductToCart(customerCart);
		verify(customerCartRepository,times(1)).save(customerCart);
	}
	
	@Test
	void updateCustomerCartTest() {
		float subTotal=1.0f;
		int quantity=1;
		int sindex=1;
		customerCartService.updateCustomerCart(subTotal, quantity,sindex);
		verify(customerCartRepository,times(1)).updateCustomerCart(subTotal, quantity, sindex);
	}
	
	
	@Test
	void getProductFromCartTest() {
		int productId=1;
		int customerId=1;
		CustomerCartDTO customerCartDTO=new CustomerCartDTO(1,1,"Books","Our Earth",123,123,1);
		when(customerCartRepository.getProductFromCart(productId, customerId)).thenReturn(customerCartDTO);
		assertEquals(customerCartDTO,customerCartService.getProductFromCart(productId, customerId));
	}

	@Test
	void getAllProductsFromCartTest() {
		int customerID=1;
        when(customerCartRepository.getAllProductsFromCustomerCart(customerID)).thenReturn(Stream
                .of(new CustomerCartDTO(1,1,"Books","Our Earth",123,123,1), new CustomerCartDTO(2,2,"Books","Our Earth",123,123,1)).collect(Collectors.toList()));
        assertEquals(2, customerCartService.getAllProductsFromCart(customerID).size());
	}
	
	@Test
	void getCartTotalPriceTest(){
		int customerId=1;
		when(customerCartRepository.getCartTotalPrice(customerId)).thenReturn(10);
		assertEquals(10,customerCartService.getCartTotalPrice(customerId));
	}
	
	@Test
	void deleteProductFromCartTest() {
		int productId=1;
		int customerId=1;
		customerCartService.deleteProductFromCart(productId,customerId);
		verify(customerCartRepository,times(1)).deleteProductFromCart(productId,customerId);
	}
	
	@Test
	void getProductsFromCustomerCartTest() {
		int customerID=1;
        when(customerCartRepository.getProductsFromCustomerCart(customerID)).thenReturn(Stream
                .of(new CustomerOrderDetails(1,"Books","Our Earth",123,123,1), new CustomerOrderDetails(2,"Books","Our Earth",123,123,1)).collect(Collectors.toList()));
        assertEquals(2, customerCartService.getProductsFromCustomerCart(customerID).size());
	}
	
	@Test
	void deleteOrderedProductFromCartTest() {
		int customerId=1;
		customerCartService.deleteOrderedProductFromCart(customerId);
		verify(customerCartRepository,times(1)).deleteOrderedProductFromCart(customerId); 
	}
	
}












