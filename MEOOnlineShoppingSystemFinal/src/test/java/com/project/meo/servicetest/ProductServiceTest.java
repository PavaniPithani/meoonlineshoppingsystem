package com.project.meo.servicetest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.project.meo.dto.ProductDTO;
import com.project.meo.entity.Product;
import com.project.meo.repository.ProductRepository;
import com.project.meo.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
class ProductServiceTest {

	@Autowired
	ProductService productService;
	@MockBean
	ProductRepository productRepository;
	
	@Test
	void addProductTest() {
		Product product=new Product(1,"Books","Our Earth",123,90,null);
		productService.addProduct(product);
		verify(productRepository,times(1)).save(product);
	}
	
	@Test
	void getAllProductsByFidTest() {
		int fid=1;
        when(productRepository.getAllProductsByFid(fid)).thenReturn(Stream
                .of(new ProductDTO(1,"Books","Our Earth",123,90), new ProductDTO(1,"Books","Our Earth",123,90)).collect(Collectors.toList()));
        assertEquals(2, productService.getAllProductsByFid(fid).size());
	}
	
	@Test
	void getAllProductsTest() {
        when(productRepository.getAllProducts()).thenReturn(Stream
                .of(new ProductDTO(1,"Books","Our Earth",123,90), new ProductDTO(1,"Books","Our Earth",123,90)).collect(Collectors.toList()));
        assertEquals(2, productService.getAllProducts().size());
	}
	
	@Test
	void getProductByIdTest() {
		int productId=1;
		ProductDTO productDTO=new ProductDTO(1,"Books","Our Earth",123,90);
		when(productRepository.getProductById(productId)).thenReturn(productDTO);
		assertEquals(productDTO,productService.getProductById(productId));
	}

	@Test
	void deleteProductTest() {
		int productId=1;
		productService.deleteProduct(productId);
		verify(productRepository,times(1)).deleteById(productId);
	}

	@Test
	void updateProductStockTest() {
		int stock=1;
		int productId=1;
		productService.updateProductStock(stock, productId);
		verify(productRepository,times(1)).updateProductStock(stock, productId);
	}
	
	@Test
	void getProductStockTest() {
		int productID=1;
		when(productRepository.getProductStock(productID)).thenReturn(1);
		assertEquals(1,productService.getProductStock(productID));
	}
	
}





