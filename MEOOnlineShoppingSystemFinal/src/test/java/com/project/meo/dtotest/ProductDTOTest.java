package com.project.meo.dtotest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.project.meo.dto.ProductDTO;

class ProductDTOTest {

	@Test
	void testProductDTO() {
		ProductDTO productDTO=new ProductDTO(1,"Books","Our Earth",123,90);
		productDTO.setProductId(1);
		productDTO.setProductCategory("Books");
		productDTO.setProductName("Our Earth");
		productDTO.setProductPrice(123);
		productDTO.setStock(90);
		assertEquals(1,productDTO.getProductId());
		assertEquals("Books",productDTO.getProductCategory());
		assertEquals("Our Earth",productDTO.getProductName());
		assertEquals(123,productDTO.getProductPrice());
		assertEquals(90,productDTO.getStock());
	}

}
