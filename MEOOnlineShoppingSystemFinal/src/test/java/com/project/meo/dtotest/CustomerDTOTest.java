package com.project.meo.dtotest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.project.meo.dto.CustomerDTO;

class CustomerDTOTest {

	@Test
	void testCustomerDTO() {
		CustomerDTO customerDTO=new CustomerDTO(1,"pavani","pavani@gmail.com",9876543212l,
				                "123456","kakinada","Andhra",433234);
		CustomerDTO customerDTO2=new CustomerDTO("pavani",9876543212l,"kakinada","Andhra",433234);
		customerDTO.setCustomerId(1);
		customerDTO2.setName("pavani");
		customerDTO.setEmailId("pavani@gmail.com");
		customerDTO.setMobileNumber(9876543212l);
		customerDTO.setPassword("123456");
		customerDTO.setCity("kakinada");
		customerDTO.setState("Andhra");
		customerDTO.setPostalCode(433234);
	
		assertEquals(1,customerDTO.getCustomerId());
		assertEquals("pavani",customerDTO.getName());
		assertEquals("pavani@gmail.com",customerDTO.getEmailId());
		assertEquals(9876543212l,customerDTO.getMobileNumber());
		assertEquals("123456",customerDTO.getPassword());
		assertEquals("kakinada",customerDTO.getCity());
		assertEquals("Andhra",customerDTO.getState());
		assertEquals(433234,customerDTO.getPostalCode());

	}

}
