package com.project.meo.dtotest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.project.meo.dto.CustomerOrderDTO;

class CustomerOrderDTOTest {

	@Test
	void testCustomerOrderDTO() {
		CustomerOrderDTO customerOrderDTO=new CustomerOrderDTO(1,200,null,null);
		customerOrderDTO.setOrderID(1);
		customerOrderDTO.setTotalAmount(200);
		customerOrderDTO.setOrderDate(null);
		customerOrderDTO.setExpecteddeliveryDate(null);
		
		assertEquals(1,customerOrderDTO.getOrderID());
		assertEquals(200,customerOrderDTO.getTotalAmount());
		assertEquals(null,customerOrderDTO.getOrderDate());
		assertEquals(null,customerOrderDTO.getExpecteddeliveryDate());
	}

}
