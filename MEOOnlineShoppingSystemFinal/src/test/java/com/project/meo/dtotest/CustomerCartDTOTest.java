package com.project.meo.dtotest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.project.meo.dto.CustomerCartDTO;

class CustomerCartDTOTest {

	@Test
	void testCustomerCartDTO() {
		CustomerCartDTO customerCartDTO=new CustomerCartDTO(1,1,"Books","Our Earth",123,123,1);
		customerCartDTO.setSindex(1);
		customerCartDTO.setProductId(1);
		customerCartDTO.setProductCategory("Books");
		customerCartDTO.setProductName("Our Earth");
		customerCartDTO.setProductPrice(123);
		customerCartDTO.setSubTotal(123);
		customerCartDTO.setQuantity(1);
		
		assertEquals(1,customerCartDTO.getSindex());
		assertEquals(1,customerCartDTO.getProductId());
		assertEquals("Books",customerCartDTO.getProductCategory());
		assertEquals("Our Earth",customerCartDTO.getProductName());
		assertEquals(123,customerCartDTO.getProductPrice());
		assertEquals(123,customerCartDTO.getSubTotal());
		assertEquals(1,customerCartDTO.getQuantity());
	
	}

}
