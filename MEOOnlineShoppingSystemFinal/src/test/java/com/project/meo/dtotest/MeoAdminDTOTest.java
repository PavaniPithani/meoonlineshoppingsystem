package com.project.meo.dtotest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.project.meo.dto.MeoAdminDTO;

class MeoAdminDTOTest {

	@Test
	void testMeoAdminDTO() {
		MeoAdminDTO meoAdminDTO=new MeoAdminDTO("pavani@gmail.com","123456");
		meoAdminDTO.setEmailId("pavani@gmail.com");
		meoAdminDTO.setPassword("123456");
		assertEquals("pavani@gmail.com",meoAdminDTO.getEmailId());
		assertEquals("123456",meoAdminDTO.getPassword());
	}

}
