package com.project.meo.dtotest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.project.meo.dto.CustomerOrderDetailsDTO;

class CustomerOrderDetailsDTOTest {

	@Test
	void testCustomerOrderDetailsDTO() {
		CustomerOrderDetailsDTO customerOrderDetailsDTO=new CustomerOrderDetailsDTO(1,1,"Books","Our Earth",123,123,1);
		customerOrderDetailsDTO.setOrderID(1);
		customerOrderDetailsDTO.setProductId(1);
		customerOrderDetailsDTO.setProductCategory("Books");
		customerOrderDetailsDTO.setProductName("Our Earth");
		customerOrderDetailsDTO.setProductPrice(123);
		customerOrderDetailsDTO.setSubTotal(123);
		customerOrderDetailsDTO.setQuantity(1);
		assertEquals(1,customerOrderDetailsDTO.getOrderID());
		assertEquals(1,customerOrderDetailsDTO.getProductId());
		assertEquals("Books",customerOrderDetailsDTO.getProductCategory());
		assertEquals("Our Earth",customerOrderDetailsDTO.getProductName());
		assertEquals(123,customerOrderDetailsDTO.getProductPrice());
		assertEquals(123,customerOrderDetailsDTO.getSubTotal());
		assertEquals(1,customerOrderDetailsDTO.getQuantity());
	}

}

