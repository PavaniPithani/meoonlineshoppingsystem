package com.project.meo.exceptiontest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.project.meo.customexception.CustomException;

class CustomExceptionTest {

	@Test
	void test() {
		CustomException c=new CustomException("valid");
		assertEquals("valid",c.getMessage());
		
	}

}
