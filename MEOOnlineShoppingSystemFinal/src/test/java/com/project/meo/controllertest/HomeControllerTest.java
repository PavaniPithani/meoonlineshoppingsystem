package com.project.meo.controllertest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.project.meo.controller.HomeController;

@SpringBootTest
class HomeControllerTest {

	@Test
	void testHomeController() {
		HomeController homeController=new HomeController();
		assertEquals("meoHome",homeController.getHome().getViewName());
		assertEquals("adminSignIn",homeController.adminSignIn().getViewName());
		assertEquals("customerSignUp",homeController.customerSignUp().getViewName());
		assertEquals("customerSignIn",homeController.customerSignIn().getViewName());
	}

}
