package com.project.meo.controllertest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import com.project.meo.controller.MeoAdminController;

class MeoAdminControllerTest {

	@Test
	void testMeoAdminController() {
		MeoAdminController meoAdminController=new MeoAdminController();
		assertEquals("meoHome",meoAdminController.adminLogOut().getViewName());
		
	}
}

