package com.project.meo.controllertest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.project.meo.controller.CustomerController;
import com.project.meo.entity.Customer;

class CustomerControllerTest {

	@Test
	void testCustomerController() {
		CustomerController customerController=new CustomerController();
		Customer customer=new Customer();
		customer.setEmailId("pavani@gmail.com");
		customerController.setCustomer(customer);
		assertEquals("customerPage",customerController.getCustomerPage().getViewName());
		assertEquals("meoHome",customerController.customerLogOut().getViewName());
	}

}
