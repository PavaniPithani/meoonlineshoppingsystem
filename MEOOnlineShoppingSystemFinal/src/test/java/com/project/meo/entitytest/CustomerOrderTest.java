package com.project.meo.entitytest;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import com.project.meo.entity.CustomerOrder;

class CustomerOrderTest {

	@Test
	void testCustomerOrder() {
		CustomerOrder customerOrder=new CustomerOrder(1,200,null,null,null,null);
		customerOrder.setOrderID(1);
		customerOrder.setTotalAmount(200);
		customerOrder.setOrderDate(null);
		customerOrder.setExpecteddeliveryDate(null);
		customerOrder.setCustomerfid(null);
		customerOrder.setOrderProductList(null);
		
		assertEquals(1,customerOrder.getOrderID());
		assertEquals(200,customerOrder.getTotalAmount());
		assertEquals(null,customerOrder.getOrderDate());
		assertEquals(null,customerOrder.getExpecteddeliveryDate());
		assertEquals(null,customerOrder.getCustomerfid());
		assertEquals(null,customerOrder.getOrderProductList());		
		
	}
	

}
