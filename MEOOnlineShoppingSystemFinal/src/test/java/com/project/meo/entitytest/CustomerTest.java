package com.project.meo.entitytest;

import org.junit.jupiter.api.Test;
import com.project.meo.entity.Customer;
import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {

	@Test
	void testCustomer() {
		Customer customer=new Customer(1,"pavani","pavani@gmail.com",9876543212l,
				"123456","kakinada","Andhra",433234,null,null);
		customer.setCustomerId(1);
		customer.setName("pavani");
		customer.setEmailId("pavani@gmail.com");
		customer.setMobileNumber(9876543212l);
		customer.setPassword("123456");
		customer.setCity("kakinada");
		customer.setState("Andhra");
		customer.setPostalCode(433234);
		customer.setCustomerCart(null);
		customer.setCustomerOrder(null);
		assertEquals(1,customer.getCustomerId());
		assertEquals("pavani",customer.getName());
		assertEquals("pavani@gmail.com",customer.getEmailId());
		assertEquals(9876543212l,customer.getMobileNumber());
		assertEquals("123456",customer.getPassword());
		assertEquals("kakinada",customer.getCity());
		assertEquals("Andhra",customer.getState());
		assertEquals(433234,customer.getPostalCode());
		assertEquals(null,customer.getCustomerCart());
		assertEquals(null,customer.getCustomerOrder());
	}

}

