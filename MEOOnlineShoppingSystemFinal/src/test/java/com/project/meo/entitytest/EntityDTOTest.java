package com.project.meo.entitytest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.project.meo.dto.CustomerDTO;
import com.project.meo.dto.ProductDTO;
import com.project.meo.entity.Customer;
import com.project.meo.entity.Product;
import com.project.meo.util.EntityDto;

class EntityDTOTest {

	@Test
	void test() {
		EntityDto entity=new EntityDto ();
		ProductDTO productDTO=new ProductDTO(1,"Books","Our Earth",123,90);
		CustomerDTO customerDTO=new CustomerDTO(1,"pavani","pavani@gmail.com",9876543212l,
                								"123456","kakinada","Andhra",433234);
		Product product=entity.productDtoEntityMapping(productDTO);
		Customer customer=entity.getCustomerDetails(customerDTO);
		assertEquals(1,product.getProductId());	
		assertEquals("pavani",customer.getName());
		
	}

}
