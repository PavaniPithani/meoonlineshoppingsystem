package com.project.meo.entitytest;

import static org.junit.jupiter.api.Assertions.*;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.project.meo.entity.MeoAdmin;
import com.project.meo.entity.Product;

class MeoAdminTest {

	List<Product> productList=new LinkedList<>();
	MeoAdmin meoAdmin;
	
	@Test
	void testMeoAdmin() {
		meoAdmin=new MeoAdmin();
		productList.add(new Product(1,"Books","Our Earth",123,90,null));
		meoAdmin=new MeoAdmin(1,"Pavani","pavani@gmail.com","123456",productList);
		assertEquals(1,meoAdmin.getAdminId());
		assertEquals("Pavani",meoAdmin.getAdminName());
		assertEquals("pavani@gmail.com",meoAdmin.getEmailId());
		assertEquals("123456",meoAdmin.getPassword());
		assertEquals(1,meoAdmin.getProducts().get(0).getProductId());
		assertEquals("Books",meoAdmin.getProducts().get(0).getProductCategory());
		assertEquals("Our Earth",meoAdmin.getProducts().get(0).getProductName());
		assertEquals(123,meoAdmin.getProducts().get(0).getProductPrice());
		assertEquals(90,meoAdmin.getProducts().get(0).getStock());
		assertEquals(null,meoAdmin.getProducts().get(0).getFid());
		meoAdmin.setAdminId(1);
		meoAdmin.setAdminName("Pavani");
		meoAdmin.setEmailId("pavani@gmail.com");
		meoAdmin.setPassword("123456");
		meoAdmin.setProducts(productList);
		
	}

}
