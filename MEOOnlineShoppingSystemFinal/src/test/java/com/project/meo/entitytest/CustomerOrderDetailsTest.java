package com.project.meo.entitytest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.project.meo.entity.CustomerOrderDetails;

class CustomerOrderDetailsTest {

	@Test
	void test() {
		CustomerOrderDetails customerOrderDetails=new CustomerOrderDetails(1,1,"Books","Our Earth",123,123,1);
		CustomerOrderDetails customerOrderDetails2=new CustomerOrderDetails(1,"Books","Our Earth",123,123,1);
		customerOrderDetails.setSno(1);
		customerOrderDetails.setProductId(1);
		customerOrderDetails.setProductCategory("Books");
		customerOrderDetails.setProductName("Our Earth");
		customerOrderDetails.setProductPrice(123);
		customerOrderDetails.setSubTotal(123);
		customerOrderDetails.setQuantity(1);
		customerOrderDetails2.setSno(1);
		assertEquals(1,customerOrderDetails.getSno());
		assertEquals(1,customerOrderDetails.getProductId());
		assertEquals("Books",customerOrderDetails.getProductCategory());
		assertEquals("Our Earth",customerOrderDetails.getProductName());
		assertEquals(123,customerOrderDetails.getProductPrice());
		assertEquals(123,customerOrderDetails.getSubTotal());
		assertEquals(1,customerOrderDetails.getQuantity());
	}

}

