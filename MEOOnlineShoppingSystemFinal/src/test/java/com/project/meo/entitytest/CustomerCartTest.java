package com.project.meo.entitytest;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import com.project.meo.entity.CustomerCart;

class CustomerCartTest {

	@Test
	void testCustomerCart() {
		CustomerCart customerCart=new CustomerCart(1,1,"Books","Our Earth",123,123,1,null);
		customerCart.setSindex(1);
		customerCart.setProductId(1);
		customerCart.setProductCategory("Books");
		customerCart.setProductName("Our Earth");
		customerCart.setProductPrice(123);
		customerCart.setSubTotal(123);
		customerCart.setQuantity(1);
		customerCart.setCustomer(null);
		
		assertEquals(1,customerCart.getSindex());
		assertEquals(1,customerCart.getProductId());
		assertEquals("Books",customerCart.getProductCategory());
		assertEquals("Our Earth",customerCart.getProductName());
		assertEquals(123,customerCart.getProductPrice());
		assertEquals(123,customerCart.getSubTotal());
		assertEquals(1,customerCart.getQuantity());
		assertEquals(null,customerCart.getCustomer());
	}

}
