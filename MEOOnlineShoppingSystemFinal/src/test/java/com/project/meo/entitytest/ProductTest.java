package com.project.meo.entitytest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.project.meo.entity.Product;

class ProductTest {

	@Test
	void testProduct() {
		Product product=new Product(1,"Books","Our Earth",123,90,null);
		product.setProductId(1);
		product.setProductCategory("Books");
		product.setProductName("Our Earth");
		product.setProductPrice(123);
		product.setStock(90);
		product.setFid(null);
		assertEquals("Books",product.getProductCategory());
		
	}

}
